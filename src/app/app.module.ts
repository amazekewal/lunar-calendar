import { BrowserModule } from '@angular/platform-browser';
import { NgModule } from '@angular/core';
// import { licham } from '../../node_modules/licham';
import { amlich } from 'amlich';
import { AppComponent } from './app.component';
import { LunerCalenderComponent } from './pages/luner-calender/luner-calender.component';
import { FormsModule } from '@angular/forms';


@NgModule({
  declarations: [
    AppComponent,
    LunerCalenderComponent
  ],
  imports: [
    FormsModule,
    BrowserModule
  ],
  providers: [],
  bootstrap: [AppComponent]
})
export class AppModule { }
