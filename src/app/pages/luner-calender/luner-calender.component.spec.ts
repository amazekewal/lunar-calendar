import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { LunerCalenderComponent } from './luner-calender.component';

describe('LunerCalenderComponent', () => {
  let component: LunerCalenderComponent;
  let fixture: ComponentFixture<LunerCalenderComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ LunerCalenderComponent ]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(LunerCalenderComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should be created', () => {
    expect(component).toBeTruthy();
  });
});
